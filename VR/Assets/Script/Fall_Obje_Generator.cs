﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fall_Obje_Generator : MonoBehaviour
{
    // Start is called before the first frame update

    public float timeOut;
    public float LifeTime;
    public float Generation_MAX_Delay_Time;
    public float Generate_X_MIN;
    public float Generate_X_MAX;
    public float Generate_Z_MIN;
    public float Generate_Z_MAX;
    
    private float timeElapsed;
    public GameObject[] FallObject;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed >= timeOut)
        {
            float x = Random.Range(Generate_X_MIN, Generate_X_MAX);
            float z = Random.Range(Generate_Z_MIN, Generate_Z_MAX);
            int number = Random.Range(0, FallObject.Length);
            GameObject FallObj = Instantiate(FallObject[number], new Vector3(x, transform.position.y, z), Quaternion.identity);
            Destroy(FallObj, LifeTime);
            timeOut = Random.Range(0, Generation_MAX_Delay_Time);
            timeElapsed = 0.0f;
        }
    }
}
