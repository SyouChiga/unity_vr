﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class PlayerCtr : MonoBehaviour
{
    //上るフラグ
    public bool OnClimb;

    //コントローラ座標
    private Vector3 RightHandPosRealTime;
    private Vector3 LeftHandPosRealTime;


    //建物をキャッチした時点のマウス座標
    private Vector3 RightHandPosOnClimb;
    private Vector3 LeftHandPosOnClimb;

    //上る時点のプレイヤー座標
    private Vector3 PlayerPosOnClimb;

    //プレイヤーの物理を取得
    private Rigidbody PlayerRigidbody_;

    [SerializeField]
    private SteamVR_Input_Sources hand_left_type_;   //!< コントローラの識別

    [SerializeField]
    private SteamVR_Action_Boolean grip_left_action_;  //!< グリップのBool値を格納する

    [SerializeField]
    private SteamVR_Input_Sources hand_right_type_;   //!< コントローラの識別

    [SerializeField]
    private SteamVR_Action_Boolean grip_right_action_;  //!< グリップのBool値を格納する

    //コントローラオブジェクト
    public GameObject RightHand;
    public GameObject LeftHand;

    //手を判定するフラグ
    private int n_HandType;

    //コントローラの座標を画面に変換する時の係数
    public float f_Up_Speed = 0.2f;

    //コントローラーの振動
    private SteamVR_Action_Vibration haptic = SteamVR_Actions._default.Haptic;

    void Start()
    {
        PlayerRigidbody_ = GetComponent<Rigidbody>();

        n_HandType = 0;
        OnClimb = false;
        Debug.Log(Physics.gravity);
    }

    private void Update()
    {
        //上り状態ではない時
        if (!OnClimb)
        {
        }

        //コントローラーの座標を常にゲットする(左右別々で)
        RightHandPosRealTime = RightHand.transform.position;
        LeftHandPosRealTime = LeftHand.transform.position;
    }

    //判定範囲に入ったら、マウスで建物を捕まえて、上り状態に入る
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "bill")
        {
            //捕まったら
            if (grip_right_action_.GetStateDown(hand_right_type_) || grip_left_action_.GetStateDown(hand_left_type_))
            {
                if (grip_right_action_.GetStateDown(hand_right_type_))
                {
                    StartGoUp(1);
                    //左手から切換されたとき左手を離させる処理が必要
                    if (n_HandType == 2)
                    {
                        StopGoUp(2);
                    }
                    n_HandType = 1;
                    haptic.Execute(0, 0.005f, 0.005f, 1, hand_right_type_);
                }
                else if (grip_left_action_.GetStateDown(hand_left_type_))
                {
                    StartGoUp(2);
                    //右から切換されたとき右手を離させる処理が必要
                    if (n_HandType == 1)
                    {
                        StopGoUp(1);
                    }
                    n_HandType = 2;
                    haptic.Execute(0, 0.005f, 0.005f, 1, hand_left_type_);
                }
            }
            //上る中
            else if (grip_right_action_.GetState(hand_right_type_) || grip_left_action_.GetState(hand_left_type_))
            {
                if (n_HandType == 1)
                {
                    GoUp(1);
                }
                if (n_HandType == 2)
                {
                    GoUp(2);
                }
            }

            //登るのを終了する(両手離したら)
            else if (!grip_left_action_.GetState(hand_left_type_) && !grip_right_action_.GetState(hand_right_type_))
            {
                Debug.Log("ビルから手を離した");
                //両手を離させる
                StopGoUp(1);
                StopGoUp(2);
                n_HandType = 3;
                //フラグ切換
                OnClimb = false;
                //重力を戻に戻す
                Physics.gravity = new Vector3(0, -9.8f, 0);
            }
        }
    }

    //建物を捕まえるかどうか判定する
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bill")
        {
            Debug.Log("捕まえる範囲に入りました");
        }
    }

    //捕まえる範囲から離した。
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "bill")
        {
            Debug.Log("捕まえる範囲から離した");
            OnClimb = false;
            //重力を戻に戻す
            Physics.gravity = new Vector3(0, 9.8f, 0);
        }
    }

    //右手で登ろうとしたら(Handが1の場合右手、2の場合左手)
    private void StartGoUp(int nHand)
    {
        //フラグ切換
        OnClimb = true;
        //重力をなしにする
        Physics.gravity = new Vector3(0, 0, 0);
        //登ろうとした瞬間のコントローラー座標をゲット
        if (nHand == 1)
        {
            RightHandPosOnClimb = RightHandPosRealTime;
        }
        else if (nHand == 2)
        {
            LeftHandPosOnClimb = LeftHandPosRealTime;
        }
        //登ろうとした瞬間にプレイヤー座標をゲット
        PlayerPosOnClimb = this.transform.position;
    }

    //登る処理(Handが1の場合右手、2の場合左手)
    private void GoUp(int nHand)
    {
        if (nHand == 1)
        {
            haptic.Execute(0, 0.005f, 0.005f, 1, hand_right_type_);
            this.transform.position = new Vector3(this.transform.position.x + (RightHandPosOnClimb.x - RightHandPosRealTime.x) * f_Up_Speed, PlayerPosOnClimb.y + (RightHandPosOnClimb.y - RightHandPosRealTime.y) * f_Up_Speed, this.transform.position.z);
        }
        else if (nHand == 2)
        {
            haptic.Execute(0, 0.005f, 0.005f, 1, hand_left_type_);
            this.transform.position = new Vector3(this.transform.position.x + (LeftHandPosOnClimb.x - LeftHandPosRealTime.x) * f_Up_Speed, PlayerPosOnClimb.y + (LeftHandPosOnClimb.y - LeftHandPosRealTime.y) * f_Up_Speed, this.transform.position.z);
        }

    }

    //登るのを停止(Handが1の場合右手、2の場合左手)
    private void StopGoUp(int nHand)
    {
        if (nHand == 1)
        {
            RightHandPosRealTime = RightHand.transform.position;
        }
        else if (nHand == 2)
        {
            LeftHandPosRealTime = LeftHand.transform.position;
        }
        //プレイヤー座標をリセット
        PlayerPosOnClimb = this.transform.position;


    }
}