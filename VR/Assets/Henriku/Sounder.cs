﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Sounder : MonoBehaviour
{
    public enum Repeat
    {
        Infinite = -1,
        Once = 1,
        Twice = 2,
        Thrice = 3
    }

    class PlayStatus
    {
        public bool paused = false;
        public bool infinite = false;
        public int queue;
        public bool playing = false;
        public float volume = 1.0f;
    }

    class SounderData
    {
        public List<PlayStatus> playStatuses = new List<PlayStatus>();
        public List<AudioClip> audioClips = new List<AudioClip>();
        public List<AudioSource> audios = new List<AudioSource>();
        public Dictionary<string, int> audioFilePathIndices = new Dictionary<string, int>();
    }

    public void Start()
    {
        Sounders.Add(soundData);
    }

    public void Update()
    {
        int audioIndex;
        for(audioIndex = 0; audioIndex < soundData.audios.Count; audioIndex++)
        {
            if (!soundData.playStatuses[audioIndex].playing || soundData.playStatuses[audioIndex].paused)
            {
                continue;
            }
            if (soundData.audios[audioIndex].isPlaying)
            {
                continue;
            }
            if (!soundData.playStatuses[audioIndex].infinite && soundData.playStatuses[audioIndex].queue <= 0)
            {
                continue;
            }
            if (soundData.playStatuses[audioIndex].queue > 0)
            {
                soundData.playStatuses[audioIndex].queue--;
            }
            soundData.audios[audioIndex].PlayOneShot(soundData.audioClips[audioIndex], soundData.playStatuses[audioIndex].volume);
        }
    }

    public static int RepeatN(int times)
    {
        return times;
    }

    public AudioClip RawClip(int audioIndex)
    {
        return soundData.audioClips[audioIndex];
    }

    public AudioSource RawSource(int audioIndex)
    {
        return soundData.audios[audioIndex];
    }

    public int PreLoad(string filePath)
    {
        int audioIndex;
        if (!soundData.audioFilePathIndices.ContainsKey(filePath))
        {
            audioIndex = soundData.audios.Count;
            soundData.audioFilePathIndices.Add(filePath, audioIndex);

            AudioClip audioClip = Resources.Load<AudioClip>(filePath);
            AudioSource audio = gameObject.AddComponent<AudioSource>();
            audio.clip = audioClip;
            audio.spatialize = true;
            audio.spatialBlend = 1.0f;

            soundData.playStatuses.Add(new PlayStatus());
            soundData.audioClips.Add(audioClip);
            soundData.audios.Add(audio);
        }
        else
        {
            audioIndex = soundData.audioFilePathIndices[filePath];
        }
        return audioIndex;
    }

    public int Play(int audioIndex, int repeat = -1, float volume = 1.0f, Vector3 offset = new Vector3())
    {
        soundData.audios[audioIndex].transform.localPosition = offset;
        soundData.audios[audioIndex].PlayOneShot(soundData.audioClips[audioIndex], volume);
        soundData.playStatuses[audioIndex].paused = false;
        soundData.playStatuses[audioIndex].infinite = repeat == -1;
        soundData.playStatuses[audioIndex].playing = true;
        soundData.playStatuses[audioIndex].queue = repeat - 1;
        soundData.playStatuses[audioIndex].volume = volume;

        return audioIndex;
    }

    public int Play(string filePath, int repeat, float volume = 1.0f, Vector3 offset = new Vector3())
    {
        int audioIndex = PreLoad(filePath);
        return Play(audioIndex, repeat, volume, offset);
    }

    public int Play(string filePath, Repeat repeat = Repeat.Infinite, float volume = 1.0f, Vector3 offset = new Vector3())
    {
        return Play(filePath, (int)repeat, volume, offset);
    }

    public void Pause(int audioIndex)
    {
        soundData.audios[audioIndex].Pause();
        soundData.playStatuses[audioIndex].paused = true;
    }

    public void UnPause(int audioIndex)
    {
        soundData.audios[audioIndex].UnPause();
        soundData.playStatuses[audioIndex].paused = false;
    }

    public void Stop(int audioIndex)
    {
        soundData.audios[audioIndex].Stop();
        soundData.playStatuses[audioIndex].playing = false;
    }

    static void PauseAll()
    {
        Sounders.ForEach((sounder) => {
            sounder.audios.ForEach((audio) => audio.Pause());
            sounder.playStatuses.ForEach((playStatus) => playStatus.paused = true);
        });
        
    }

    static void StopAll()
    {
        Sounders.ForEach((sounder) =>
        {
            sounder.audios.ForEach((audio) => audio.Stop());
            sounder.playStatuses.ForEach((playStatus) => playStatus.playing = false);
        });
    }

    SounderData soundData = new SounderData();
    static List<SounderData> Sounders = new List<SounderData>();
    static uint maxVoices = 32;
}
