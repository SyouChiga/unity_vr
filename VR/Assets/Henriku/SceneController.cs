﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public string audioFilePath;
    public Sounder sounder;

    // Start is called before the first frame update
    void Start()
    {
        sounder.Play(audioFilePath, Sounder.Repeat.Infinite, 1.0f, new Vector3(0.0f,10.0f, 0.0f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
