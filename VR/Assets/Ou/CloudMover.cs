﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMover : MonoBehaviour
{
    private float x;
    private float z;
    public float MaxX;  //X軸移動開始座標
    public float MinX;  //X軸逆移動開始座標
    public float MaxZ;  //Z軸移動開始座標
    public float MinZ;  //Z軸逆移動開始座標

    //移動しない時チェックを外す
    public bool moveX;  //X軸移動時チェック
    public bool revX;   //X軸逆移動時チェック
    public bool moveZ;  //Z軸移動時チェック
    public bool revZ;   //Z軸逆移動時チェック

    // Start is called before the first frame update
    void Start()
    {
        if (moveX == true & revX == false)
        {
            x = MaxX;
        }
        if (moveZ == true & revZ == false)
        {
            z = MaxZ;
        }
        if (moveX == false & revX == true)
        {
            x = MinX;
        }
        if (moveZ == false & revZ == true)
        {
            z = MinZ;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //X軸移動

        if (moveX == true && revX == false) 
        {
            if (x <= MaxX)
            {
                x -= 0.1f;
            }
            if (x <= -MaxX)
            {
                x = MaxX;
            }
            this.gameObject.transform.position = new Vector3(x, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
        }

        if (moveX == false && revX == true)
        {
            if (x >= MinX)
            {
                x += 0.1f;
            }
            if (x >= -MinX)
            {
                x = MinX;
            }
            this.gameObject.transform.position = new Vector3(x, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
        }
        //Z軸移動

        if (moveZ == true && revZ == false) 
        {
            if (z <= MaxZ)
            {
                z -= 0.1f;
            }
            if (z <= -MaxZ)
            {
                z = MaxZ;
            }
            this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, z);
        }

        if (moveZ == false && revZ == true)
        {
            if (z >= MinZ)
            {
                z += 0.1f;
            }
            if (z >= -MinZ)
            {
                z = MinZ;
            }
            this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, z);
        }
    }
}
